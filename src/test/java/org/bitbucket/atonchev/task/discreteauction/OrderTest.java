package org.bitbucket.atonchev.task.discreteauction;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.bitbucket.atonchev.task.discreteauction.Order.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;

import static org.junit.Assert.assertTrue;

@RunWith(Theories.class)
public class OrderTest {
	@DataPoints
	public static String[] INVALID = {"A 233454 10354.00", "B vdfgt 1.001",
			"54093 34432", "B 145654 100 543", "B 145654.09 100", 
			"S 145654 100\nS 145654 100", "B -200 100", "S 200 -10.10"};
	
	@Test
	public void testParse() {
		Order order;
		
		order = Order.parse("B 145654 100");
		assertEquals(Direction.BUY, order.direction);
		assertEquals(BigInteger.valueOf(145654L), order.quantity);
		assertTrue(BigDecimal.valueOf(100d).compareTo(order.price) == 0);
		
		order = Order.parse("    S 145654473492389230000   100.000000000000000000000000000000001    ");
		assertEquals(Direction.SELL, order.direction);
		assertEquals(new BigInteger("145654473492389230000"), order.quantity);
		assertTrue(new BigDecimal("100.000000000000000000000000000000001")
				.compareTo(order.price) == 0);
	}
	
	@Test(expected = NullPointerException.class)
	public void testNpe() {
		Order.parse(null);
	}
	
	
	@Theory
	public void testInvalidInput(String line) {
		Exception e = null;
		try {
			Order.parse(line);
		} catch (Exception c) {
			e = c;
		}
		assertTrue(IllegalArgumentException.class.isInstance(e));
	}
}
