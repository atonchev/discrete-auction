package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.assertTrue;
import org.bitbucket.atonchev.task.discreteauction.Order;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class OrderAggregationTaskTest {
	@DataPoints
	public static String[] INVALID = {"B 1 2134", "S 1001 50.01", "S 100 0.50",
			"B 100 0.501"};
	
	@Test
	public void testCheckConstraints() {
		Order order = Order.parse("S 200 55.20");
		OrderAggregationTask.checkConstraints(order);
	}
	
	@Theory
	public void testCheckConstraints(String invalid) {
		Order order = Order.parse(invalid);
		Exception e = null;
		try {
			OrderAggregationTask.checkConstraints(order);
		} catch (Exception c) {
			e = c;
		}
		assertTrue(IllegalArgumentException.class.isInstance(e));
	}
}
