package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OrderAggregationTaskRunTest {
	private LongAdder[] supply = NaiveStrategy.createArray();
	private LongAdder[] demand = NaiveStrategy.createArray();
	
	@Mock
	private Runnable onSuccess;
	@Mock
	private Consumer<Exception> onException;
	
	@Test
	public void testBuy() {
		String[] lines = {"B 10 1.01", "B 10 10.00", "B 10 20.25"};
		
		for (String line : lines) {
			OrderAggregationTask task = new OrderAggregationTask(line, supply,
					demand, onSuccess, onException);
			task.run();
		}
		assertEquals(30L, demand[NaiveStrategy.MIN_PRICE_SLOT].sum());
		assertEquals(30L, demand[101].sum());
		assertEquals(20L, demand[1000].sum());
		assertEquals(10L, demand[2025].sum());
		assertEquals( 0L, demand[NaiveStrategy.MAX_PRICE_SLOT].sum());
		
		verify(onSuccess, times(3)).run();
		verify(onException, never()).accept(any());
	}
	
	@Test
	public void testSell() {
		String[] lines = {"S 10 99.00", "S 10 50.33", "S 10 50.00"};
		
		for (String line : lines) {
			OrderAggregationTask task = new OrderAggregationTask(line, supply,
					demand, onSuccess, onException);
			task.run();
		}
		assertEquals( 0L, supply[NaiveStrategy.MIN_PRICE_SLOT].sum());
		assertEquals(10L, supply[5000].sum());
		assertEquals(20L, supply[5033].sum());
		assertEquals(30L, supply[9999].sum());
		assertEquals(30L, supply[NaiveStrategy.MAX_PRICE_SLOT].sum());
		
		verify(onSuccess, times(3)).run();
		verify(onException, never()).accept(any());
	}
	
	@Test
	public void testExceptionCallback() {
		String[] lines = {"S 1001 50.33"};
		AtomicReference<Exception> exHolder = new AtomicReference<>(null);
		
		for (String line : lines) {
			OrderAggregationTask task = new OrderAggregationTask(line, supply,
					demand, onSuccess, exHolder::set);
			task.run();
		}
		
		verify(onSuccess, never()).run();
		assertNotNull(exHolder.get());
		assertEquals("S 1001 50.33", exHolder.get().getMessage());
	}
}
