package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

import org.junit.Test;

public class OrderAggregatorTest {
	private LongAdder[] supply = NaiveStrategy.createArray();
	private LongAdder[] demand = NaiveStrategy.createArray();
	
	@Test
	public void testAggregate() throws Exception{
		ExecutorService executor = Executors.newSingleThreadExecutor();
		String[] orders = {
				"B 10 1.01",
				"S 10 99.00",
				"B 10 10.00",
				"S 10 50.33",
				"S 10 50.00",
				"B 10 20.25"
		};
		OrderAggregator aggregator = new OrderAggregator(supply, demand, executor);
		aggregator.aggregate(testData(orders));
		
		assertEquals(30L, demand[NaiveStrategy.MIN_PRICE_SLOT].sum());
		assertEquals(30L, demand[101].sum());
		assertEquals(20L, demand[1000].sum());
		assertEquals(10L, demand[2025].sum());
		assertEquals( 0L, demand[NaiveStrategy.MAX_PRICE_SLOT].sum());
		
		assertEquals( 0L, supply[NaiveStrategy.MIN_PRICE_SLOT].sum());
		assertEquals(10L, supply[5000].sum());
		assertEquals(20L, supply[5033].sum());
		assertEquals(30L, supply[9999].sum());
		assertEquals(30L, supply[NaiveStrategy.MAX_PRICE_SLOT].sum());
	}
	
	@Test
	public void testExceptionAfterSubmitting() {
		/*
		 * DelayedStartQueue doesn't not allow the executor to start processing
		 * until the aggregator reaches the end of the inputStream. This helps
		 * us test that the aggregator will know about the exception after it
		 * has finished submitting tasks. 
		 */
		final CountDownLatch latch = new CountDownLatch(1);
		ExecutorService executor = new ThreadPoolExecutor(1, 1, 0,
				TimeUnit.MILLISECONDS, new DelayedStartQueue<>(10, latch));
		String[] orders = {
				"B 10 10",
				"S 1001 10",
				"B 10 10",
				"S 10 10",
				"B 10 10",
				"S 10 10"
		};
		OrderAggregator aggregator = new OrderAggregator(supply, demand, executor);
		
		InputStream specialStream = new SignalOnEndInputStream(testData(orders),
				latch::countDown);
		
		Exception e = null;
		try {
			aggregator.aggregate(specialStream);
		} catch (Exception c) {
			e = c;
		}
		assertNotNull(e);
		assertEquals("S 1001 10", e.getMessage());
		
		executor.shutdownNow();
	}
	
	private static InputStream testData(String[] orders) {
		return new ByteArrayInputStream(String.join("\n", orders).getBytes());
	}
	
	static class SignalOnEndInputStream extends FilterInputStream {
		private final Runnable onEnd;
		
		public SignalOnEndInputStream(InputStream in, Runnable onEnd) {
			super(in);
			this.onEnd = onEnd;
		}

		@Override
		public int read() throws IOException {
			int result = super.read();
			if (result == -1) {
				this.onEnd.run();
			}
			return result;
		}

		@Override
		public int read(byte[] b) throws IOException {
			int result = super.read(b);
			if (result == -1) {
				this.onEnd.run();
			}
			return result;
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			int result = super.read(b, off, len);
			if (result == -1) {
				this.onEnd.run();
			}
			return result;
		}
	}
	
	static class DelayedStartQueue<E> extends ArrayBlockingQueue<E> {
		private static final long serialVersionUID = -4990004414829516116L;
		private final CountDownLatch latch;

		public DelayedStartQueue(int capacity, CountDownLatch latch) {
			super(capacity);
			this.latch = latch;
		}
		
		@Override
		public E take() throws InterruptedException {
			latch.await();
			return super.take();
		}
		
		@Override
		public E poll() {
			for (;;) {
				try {
					latch.await();
					return super.poll();
				} catch (InterruptedException retry) {}
			}
		}
		
		@Override
		public E poll(long timeout, TimeUnit unit) throws InterruptedException {
			latch.await();
			return super.poll(timeout, unit);
		}
	}
}
