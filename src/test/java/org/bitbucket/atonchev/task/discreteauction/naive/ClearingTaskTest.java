package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.LongAdder;

import org.junit.Test;

public class ClearingTaskTest {
	private final int lowerBound = 1;
	private final int upperBound = 10;
	
	@Test
	public void testClearingTask() throws Exception {
		int[] demandValues = {80, 80, 60, 50, 30, 30, 20, 20, 10, 10};
		int[] supplyValues = {10, 10, 20, 30, 30, 40, 40, 50, 60, 70};
		
		LongAdder[] demand = new LongAdder[upperBound + 1];
		LongAdder[] supply = new LongAdder[upperBound + 1];
		
		for (int i = 1; i <= upperBound; i++) {
			demand[i] = new LongAdder();
			demand[i].add(demandValues[i - 1]);
			supply[i] = new LongAdder();
			supply[i].add(supplyValues[i - 1]);
		}
		
		ClearingTask task = new ClearingTask(supply, demand, lowerBound, upperBound);
		ClearingPriceRange range = task.call();
		
		assertEquals(30, range.quantity);
		assertEquals(4, range.minPriceSlot);
		assertEquals(6, range.maxPriceSlot);
	}
	
	@Test
	public void testNoClearing() throws Exception {
		int[] demandValues = {80, 60, 40, 20, 0, 0, 0,  0,  0,  0};
		int[] supplyValues = { 0,  0,  0,  0, 0, 0, 0, 10, 30, 50};
		
		LongAdder[] demand = new LongAdder[upperBound + 1];
		LongAdder[] supply = new LongAdder[upperBound + 1];
		
		for (int i = 1; i <= upperBound; i++) {
			demand[i] = new LongAdder();
			demand[i].add(demandValues[i - 1]);
			supply[i] = new LongAdder();
			supply[i].add(supplyValues[i - 1]);
		}
		
		ClearingTask task = new ClearingTask(supply, demand, lowerBound, upperBound);
		ClearingPriceRange range = task.call();
		
		assertEquals(0, range.quantity);
	}
	
	@Test
	public void testFullClearing() throws Exception {
		int[] demandValues = {100, 90, 80, 70, 70, 60, 60, 50, 50, 50};
		int[] supplyValues = { 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};
		
		LongAdder[] demand = new LongAdder[upperBound + 1];
		LongAdder[] supply = new LongAdder[upperBound + 1];
		
		for (int i = 1; i <= upperBound; i++) {
			demand[i] = new LongAdder();
			demand[i].add(demandValues[i - 1]);
			supply[i] = new LongAdder();
			supply[i].add(supplyValues[i - 1]);
		}
		
		ClearingTask task = new ClearingTask(supply, demand, lowerBound, upperBound);
		ClearingPriceRange range = task.call();
		
		assertEquals(40, range.quantity);
		assertEquals(1, range.minPriceSlot);
		assertEquals(10, range.maxPriceSlot);
	}
}
