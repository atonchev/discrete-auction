package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.*;
import java.util.List;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class ClearingPriceFinderTest {
	@DataPoints
	public static int[] PARTS = {2, 3, 4, 5};
	
	@Test
	public void testDivide() {
		List<int[]> bounds = ClearingPriceFinder.divide(1);
		assertEquals(1, bounds.size());
		assertEquals(NaiveStrategy.MIN_PRICE_SLOT, bounds.get(0)[0]);
		assertEquals(NaiveStrategy.MAX_PRICE_SLOT, bounds.get(0)[1]);
	}
	
	@Theory
	public void testDivide(int parts) {
		List<int[]> bounds = ClearingPriceFinder.divide(parts);
		
		assertEquals(parts, bounds.size());
		assertEquals(NaiveStrategy.MIN_PRICE_SLOT, bounds.get(0)[0]);
		
		for (int i = 1; i < bounds.size(); i++) {
			assertTrue(bounds.get(i)[0] == bounds.get(i - 1)[1]);
		}
		
		assertEquals(NaiveStrategy.MAX_PRICE_SLOT, bounds.get(parts - 1)[1]);
	}
}
