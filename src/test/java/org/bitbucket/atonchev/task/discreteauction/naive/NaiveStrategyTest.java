package org.bitbucket.atonchev.task.discreteauction.naive;

import org.bitbucket.atonchev.task.discreteauction.Clearing;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NaiveStrategyTest {
	
	@Test
	public void testClearingAvgPrice() {
		ClearingPriceRange arg;
		Clearing actual; 
		
		arg = new ClearingPriceRange(0, 100, 1000);
		actual = NaiveStrategy.clearingAvgPrice(arg);
		assertEquals(0, actual.quantity.longValue());
		
		arg = new ClearingPriceRange(10, 1101, 1234);
		actual = NaiveStrategy.clearingAvgPrice(arg);
		assertEquals(2, actual.price.scale());
		assertEquals(11.68d, actual.price.doubleValue(), 0.001d);
		assertEquals(10, actual.quantity.longValue());
		
		arg = new ClearingPriceRange(1_000_000, 1234, 1234);
		actual = NaiveStrategy.clearingAvgPrice(arg);
		assertEquals(2, actual.price.scale());
		assertEquals(12.34d, actual.price.doubleValue(), 0.001d);
		assertEquals(1_000_000L, actual.quantity.longValue());
	}
	
	@Test
	public void testWithSavedData() throws Exception {
		NaiveStrategy strategy = new NaiveStrategy(4);
		Clearing clearing = strategy.clear(this.getClass().getResourceAsStream("/data.txt"));
		assertEquals(new BigInteger("64333"), clearing.quantity);
		assertEquals(new BigDecimal("53.60"), clearing.price);
	}
}
