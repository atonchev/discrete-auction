package org.bitbucket.atonchev.task.discreteauction.naive;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClearingPriceRangeTest {
	
	@Test
	public void testMergeBest() {
		ClearingPriceRange a = new ClearingPriceRange(50, 101, 200);
		ClearingPriceRange b = new ClearingPriceRange(40, 55, 250);
		ClearingPriceRange c = ClearingPriceRange.mergeBest(a, b);
		assertEquals(50, c.quantity);
		assertEquals(101, c.minPriceSlot);
		assertEquals(200, c.maxPriceSlot);
		
		a = new ClearingPriceRange(50, 101, 200);
		b = new ClearingPriceRange(50, 120, 250);
		c = ClearingPriceRange.mergeBest(a, b);
		assertEquals(50, c.quantity);
		assertEquals(101, c.minPriceSlot);
		assertEquals(250, c.maxPriceSlot);
	}
}
