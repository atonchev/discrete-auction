package org.bitbucket.atonchev.task.discreteauction.naive;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;

class OrderAggregator {
	private final LongAdder[] supply;
	private final LongAdder[] demand;
	private final LongAdder processedOrderCount = new LongAdder();
	private final AtomicBoolean exceptionFlag = new AtomicBoolean(false);
	private final AtomicReference<Exception> exHolder = new AtomicReference<>(null);
	private final Executor executor;
	
	OrderAggregator(LongAdder[] supply, LongAdder[] demand, Executor executor) {
		this.demand = demand;
		this.supply = supply;
		this.executor = executor;
	}
	
	void aggregate(InputStream inputStream) throws Exception {
		try (Scanner scanner = new Scanner(new BufferedInputStream(inputStream))) {
			long orderCount = 0;
			
			while (scanner.hasNextLine()) {
				if (exceptionFlag.get()) {
					throw exHolder.get();
				}
				Runnable task = new OrderAggregationTask(scanner.nextLine(), supply, demand,
						this::onSuccess, this::onException);
				executor.execute(task);
				orderCount++;
			}
			// CompletionService?
			while (processedOrderCount.sum() < orderCount) {
				if (exceptionFlag.get()) {
					throw exHolder.get();
				}
				Thread.sleep(1);
			}
		}
	}
	
	void onSuccess() {
		processedOrderCount.increment();
	}
	
	void onException(Exception e) {
		exHolder.compareAndSet(null, e);
		exceptionFlag.set(true);
	}
}
