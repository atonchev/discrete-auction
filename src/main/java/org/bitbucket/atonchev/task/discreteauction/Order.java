package org.bitbucket.atonchev.task.discreteauction;

import java.math.BigDecimal;
import java.math.BigInteger;

public final class Order {
	public final BigInteger quantity;
	public final BigDecimal price;
	public final Direction direction;
		
	public Order(Direction direction, BigInteger quantity, BigDecimal price) {
		if (direction == null || quantity == null || price == null) {
			throw new NullPointerException();
		}
		this.direction = direction;
		this.quantity = quantity;
		this.price = price;
	}

	public enum Direction { BUY, SELL }
	
	public static Order parse(String line) throws IllegalArgumentException {
		String[] fields = split(line.trim());
		return new Order(direction(fields[0]),
				quantity(fields[1]),
				price(fields[2]));
	}
	
	private static String[] split(String line) {
		String[] fields = line.split(" +");
		if (fields.length != 3) {
			throw new IllegalArgumentException(String.valueOf(fields.length));
		}
		return fields;
	}
	
	private static Direction direction(String arg) {
		if (arg.equals("B")) {
			return Direction.BUY;
		} else if (arg.equals("S")) {
			return Direction.SELL;
		} else {
			throw new IllegalArgumentException(arg);
		}
	}
	
	private static BigInteger quantity(String arg) {
		BigInteger q = new BigInteger(arg);
		if (q.compareTo(BigInteger.ZERO) < 0 ||
				q.compareTo(BigInteger.ZERO) == 0) {
			throw new IllegalArgumentException(arg);
		}
		return q;
	}
	
	private static BigDecimal price(String arg) {
		BigDecimal p = new BigDecimal(arg);
		if (p.compareTo(BigDecimal.ZERO) < 0 ||
				p.compareTo(BigDecimal.ZERO) == 0) {
			throw new IllegalArgumentException(arg);
		}
		return p;
	}
}
