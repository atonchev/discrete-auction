package org.bitbucket.atonchev.task.discreteauction.naive;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.atomic.LongAdder;

class ClearingPriceFinder {
	private final Executor executor;
	private final LongAdder[] supply;
	private final LongAdder[] demand;
	private final int parallelism;
	
	ClearingPriceFinder(LongAdder[] supply, LongAdder[] demand,
			Executor executor, int parallelism) {
		this.executor = executor;
		this.supply = supply;
		this.demand = demand;
		this.parallelism = parallelism;
	}
	
	ClearingPriceRange find() throws Exception {
		CompletionService<ClearingPriceRange> service =
				new ExecutorCompletionService<>(executor);
		
		for (int[] bounds : divide(parallelism)) {
			service.submit(new ClearingTask(supply, demand, bounds[0], bounds[1]));
		}
		
		ClearingPriceRange result = new ClearingPriceRange(0, 0, 0);
		for (int i = 0; i < parallelism; i++) {
			ClearingPriceRange clearing = service.take().get();
			result = ClearingPriceRange.mergeBest(result, clearing);
		}
		
		return result;
	}
	
	static List<int[]> divide(int parts) {
		int width = 1 + (NaiveStrategy.MAX_PRICE_SLOT -
				NaiveStrategy.MIN_PRICE_SLOT) / parts;
		List<int[]> result = new ArrayList<>(parts);
		
		for (int i = 0; i < parts; i++) {
			int lowerBound = NaiveStrategy.MIN_PRICE_SLOT + i * width;
			int upperBound = Integer.min(lowerBound + width, NaiveStrategy.MAX_PRICE_SLOT);
			result.add(new int[]{lowerBound, upperBound});
		}
		
		return result;
	}
}
