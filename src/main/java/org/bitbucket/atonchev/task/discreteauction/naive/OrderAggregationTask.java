package org.bitbucket.atonchev.task.discreteauction.naive;

import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;

import org.bitbucket.atonchev.task.discreteauction.Order;
import org.bitbucket.atonchev.task.discreteauction.Order.Direction;

class OrderAggregationTask implements Runnable {
	private final String line;
	private final LongAdder[] supply;
	private final LongAdder[] demand;
	private final Runnable onSuccess;
	private final Consumer<Exception> onException;
	
	OrderAggregationTask(String line, LongAdder[] supply, LongAdder[] demand,
			Runnable onSuccess, Consumer<Exception> onException) {
		this.line = line;
		this.supply = supply;
		this.demand = demand;
		this.onSuccess = onSuccess;
		this.onException = onException;
	}

	@Override
	public void run() {
		try {
			Order order = Order.parse(line);
			checkConstraints(order);
			boolean buy = order.direction == Direction.BUY;
			long quantity = order.quantity.longValue();
			int priceSlot = order.price
					.movePointRight(NaiveStrategy.PRICE_PRECISION).intValue();
			
			if (run(buy, quantity, priceSlot)) {
				onSuccess.run();
			}
			
		} catch (Exception e) {
			Exception wrapperEx = new Exception(line, e);
			onException.accept(wrapperEx);
		}
	}
	
	private boolean run(boolean buy, long quantity, int priceSlot) {
		if (buy) {
			int bound = NaiveStrategy.MIN_PRICE_SLOT;
			for (int i = priceSlot; i >= bound; i--) {
				if (Thread.currentThread().isInterrupted()) {
					return false;
				}
				demand[i].add(quantity);
			}
		} else {
			int bound = NaiveStrategy.MAX_PRICE_SLOT;
			for (int i = priceSlot; i <= bound; i++) {
				if (Thread.currentThread().isInterrupted()) {
					return false;
				}
				supply[i].add(quantity);
			}
		}
		return true;
	}
	
	static void checkConstraints(Order order) {
		if (order.quantity.compareTo(NaiveStrategy.MIN_QUANTITY) < 0 ||
				order.quantity.compareTo(NaiveStrategy.MAX_QUANTITY) > 0) {
			throw new IllegalArgumentException(order.quantity.toString());
		}
		
		if (order.price.compareTo(NaiveStrategy.MIN_PRICE) < 0 ||
				order.price.compareTo(NaiveStrategy.MAX_PRICE) > 0) {
			throw new IllegalArgumentException(order.price.toString());
		}
		
		if (order.price.scale() > NaiveStrategy.PRICE_PRECISION) {
			throw new IllegalArgumentException(order.price.toString());
		}
	}
}
