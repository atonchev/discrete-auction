package org.bitbucket.atonchev.task.discreteauction;

import java.io.InputStream;

public interface ClearingImplementationStrategy {
	
	public Clearing clear(InputStream ordersInputStream) throws Exception;
}
