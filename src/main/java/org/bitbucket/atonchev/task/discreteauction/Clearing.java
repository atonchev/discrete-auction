package org.bitbucket.atonchev.task.discreteauction;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Clearing {
	public final BigInteger quantity;
	public final BigDecimal price;
	
	public Clearing(BigInteger quantity, BigDecimal price) {
		if (quantity == null || price == null) {
			throw new NullPointerException();
		}
		this.quantity = quantity;
		this.price = price;
	}
	
	@Override
	public String toString() {
		if (quantity.equals(BigInteger.ZERO)) {
			return "0 n/a";
		}
		return quantity.toString() + " " + price.toPlainString();
	}
}
