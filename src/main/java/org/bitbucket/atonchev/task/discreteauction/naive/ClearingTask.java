package org.bitbucket.atonchev.task.discreteauction.naive;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.LongAdder;

class ClearingTask implements Callable<ClearingPriceRange> {
	private final LongAdder[] supply;
	private final LongAdder[] demand;
	private final int lowerBound;
	private final int upperBound;
	
	public ClearingTask(LongAdder[] supply, LongAdder[] demand,
			int lowerBound, int upperBound) {
		this.supply = supply;
		this.demand = demand;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	@Override
	public ClearingPriceRange call() throws Exception {
		long quantity = 0;
		long maxQuantity = 0;
		
		int minPriceSlot = lowerBound;
		int maxPriceSlot = lowerBound;
		
		for (int i = lowerBound; i <= upperBound; i++) {
			quantity = Long.min(supply[i].sum(), demand[i].sum());
			if (quantity < maxQuantity) {
				break;
			}
			if (quantity == maxQuantity) {
				maxPriceSlot = i;
			}
			if (quantity > maxQuantity) {
				maxQuantity = quantity;
				minPriceSlot = i;
				maxPriceSlot = i;
			}
		}
		
		return new ClearingPriceRange(maxQuantity, minPriceSlot, maxPriceSlot);
	}
}
