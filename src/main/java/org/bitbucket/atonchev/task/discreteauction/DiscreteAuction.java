package org.bitbucket.atonchev.task.discreteauction;

import org.bitbucket.atonchev.task.discreteauction.naive.NaiveStrategy;

public class DiscreteAuction {
	
	public static void main(String[] args) {
		int parallelism = Integer.parseInt(System.getProperty("parallelism", "2"));
		ClearingImplementationStrategy strategy = new NaiveStrategy(parallelism);
		
		try {
			Clearing clearing = strategy.clear(System.in);
			System.out.println(clearing);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
