package org.bitbucket.atonchev.task.discreteauction.naive;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.LongAdder;
import org.bitbucket.atonchev.task.discreteauction.Clearing;
import org.bitbucket.atonchev.task.discreteauction.ClearingImplementationStrategy;

public class NaiveStrategy implements ClearingImplementationStrategy {
	// input data constraints
	public static final BigDecimal MIN_PRICE = BigDecimal.valueOf(1);
	public static final BigDecimal MAX_PRICE = BigDecimal.valueOf(100);
	public static final BigInteger MIN_QUANTITY = BigInteger.valueOf(1);
	public static final BigInteger MAX_QUANTITY = BigInteger.valueOf(1000);
	public static final int PRICE_PRECISION = 2;
	public static final int ORDER_LIMIT = 1_000_000;
	
	static final int MIN_PRICE_SLOT = MIN_PRICE.intValue() * 100;
	static final int MAX_PRICE_SLOT = MAX_PRICE.intValue() * 100;

	private final int parallelism;
	
	public NaiveStrategy(int parallelism) {
		this.parallelism = parallelism;
	}
	
	public Clearing clear(InputStream inputStream) throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(parallelism);
		LongAdder[] supply = createArray();
		LongAdder[] demand = createArray();
		
		try {
			OrderAggregator aggregator =
					new OrderAggregator(supply, demand, executor);
			aggregator.aggregate(inputStream);
			
			ClearingPriceFinder finder =
					new ClearingPriceFinder(supply, demand, executor, parallelism);
			ClearingPriceRange range = finder.find();
			
			return clearingAvgPrice(range);
			
		} finally {
			executor.shutdownNow();
		}
	}
	
	static Clearing clearingAvgPrice(ClearingPriceRange range) {
		if (range.quantity == 0) {
			return new Clearing(BigInteger.ZERO, BigDecimal.ZERO);
		}
		
		BigInteger quantity = BigInteger.valueOf(range.quantity);
		BigDecimal minPrice = BigDecimal.valueOf(range.minPriceSlot).movePointLeft(2);
		BigDecimal maxPrice = BigDecimal.valueOf(range.maxPriceSlot).movePointLeft(2);
		
		BigDecimal avgPrice = minPrice.add(maxPrice)
				.divide(BigDecimal.valueOf(2), BigDecimal.ROUND_UP);
		
		return new Clearing(quantity, avgPrice);
	}
	
	static LongAdder[] createArray() {
		// yeah, 1% of array is wasted but it makes everything else simpler
		LongAdder[] arr = new LongAdder[MAX_PRICE_SLOT + 1];
		Arrays.setAll(arr, (arg) -> {return new LongAdder();});
		return arr;
	}
}
