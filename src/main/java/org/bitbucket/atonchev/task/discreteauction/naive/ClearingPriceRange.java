package org.bitbucket.atonchev.task.discreteauction.naive;

class ClearingPriceRange {
	public final long quantity;
	public final int minPriceSlot;
	public final int maxPriceSlot;
	
	public ClearingPriceRange(long quantity, int minPriceSlot, int maxPriceSlot) {
		this.quantity = quantity;
		this.minPriceSlot = minPriceSlot;
		this.maxPriceSlot = maxPriceSlot;
	}
	
	static ClearingPriceRange mergeBest(ClearingPriceRange a, ClearingPriceRange b) {
		if (a.quantity > b.quantity) {
			return a;
		}
		if (a.quantity < b.quantity) {
			return b;
		}
		int minSlot = Integer.min(a.minPriceSlot, b.minPriceSlot);
		int maxSlot = Integer.max(a.maxPriceSlot, b.maxPriceSlot);
		return new ClearingPriceRange(a.quantity, minSlot, maxSlot);
	}
}
